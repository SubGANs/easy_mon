#!/usr/bin/env python

# Copyright (c) 2009, Giampaolo Rodola'. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import socket

import psutil


af_map = {
    socket.AF_INET: 'IPv4',
    socket.AF_INET6: 'IPv6',
    psutil.AF_LINK: 'MAC',
}

duplex_map = {
    psutil.NIC_DUPLEX_FULL: "full",
    psutil.NIC_DUPLEX_HALF: "half",
    psutil.NIC_DUPLEX_UNKNOWN: "?",
}


def bytes2human(n):
    """
    >>> bytes2human(10000)
    '9.8 K'
    >>> bytes2human(100001221)
    '95.4 M'
    """
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.2f%s' % (value, s)
    return '%.2fB' % (n)


def main():
    stats = psutil.net_if_stats()
    io_counters = psutil.net_io_counters(pernic=True)
    interfaces = {}
    for nic, addrs in psutil.net_if_addrs().items():
        if nic in stats:
            st = stats[nic]
            interfaces[nic] = {
                'speed': '%sMB' % st.speed,
                'duplex': '%s' % duplex_map[st.duplex],
                'mtu': '%s' % st.mtu,
                'up': '%s' % "yes" if st.isup else "no"
            }

        if nic in io_counters:
            io = io_counters[nic]
            interfaces[nic]['incoming'] = {
                'bytes': '%s' % bytes2human(io.bytes_recv),
                'pkts': '%s' % io.packets_recv,
                'errs': '%s' % io.errin,
                'drops': '%s' % io.dropin
            }
            interfaces[nic]['outgoing'] = {
                'bytes': '%s' % bytes2human(io.bytes_sent),
                'pkts': '%s' % io.packets_sent,
                'errs': '%s' % io.errout,
                'drops': '%s' % io.dropout
            }
        interfaces[nic]['addrs'] = {}
        for index, addr in enumerate(addrs):
            interfaces[nic]['addrs'].update({
                index: {
                    'family': af_map.get(addr.family, addr.family),
                    'address': addr.address
                }
            })
            if addr.broadcast:
                interfaces[nic]['addrs'][index].update({'broadcast': addr.broadcast})
            if addr.netmask:
                interfaces[nic]['addrs'][index].update({'netmask': addr.netmask})
            if addr.ptp:
                interfaces[nic]['addrs'][index].update({'ptp': addr.ptp})
    return interfaces
