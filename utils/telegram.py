#!/usr/bin/env python3
import requests
import os
import sqlite3


DB_PATH = os.path.dirname(os.path.abspath(__file__)) + "/../data.db"


def send_message(token, chat_id, text, proxy, proxy_proto,
                 proxy_server, proxy_port, proxy_login, proxy_pass):
    data = {'chat_id': chat_id, 'text': text, 'parse_mode': 'HTML'}

    try:
        if proxy is 1:
            proxies = {
                'http': '{proxy_proto}://{proxy_login}:{proxy_pass}@{proxy_server}:{proxy_port}'.format(
                    proxy_proto=proxy_proto, proxy_login=proxy_login, proxy_pass=proxy_pass,
                    proxy_server=proxy_server, proxy_port=proxy_port),
                'https': '{proxy_proto}://{proxy_login}:{proxy_pass}@{proxy_server}:{proxy_port}'.format(
                    proxy_proto=proxy_proto, proxy_login=proxy_login, proxy_pass=proxy_pass,
                    proxy_server=proxy_server, proxy_port=proxy_port)
            }
            r = requests.post("https://api.telegram.org/bot{}/sendMessage".format(token),
                              proxies=proxies, data=data, timeout=10)
            if r.json()['ok'] is True:
                return 1, ""
            else:
                return 0, "{}: {} - {}".format(chat_id, r.json()['error_code'],
                                               r.json()['description'])
        else:
            r = requests.post("https://api.telegram.org/bot{}/sendMessage".format(token), data=data,
                              timeout=10)
            if r.json()['ok'] is True:
                return 1, ""
            else:
                return 0, "{}: {} - {}".format(chat_id, r.json()['error_code'],
                                               r.json()['description'])
    except OSError:
        return 0, "{}: Connection error".format(chat_id)


def run(event_id, hostname, text):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    sql = """SELECT status, token, proxy_status, proxy_proto, proxy_server,
                    proxy_port, proxy_login, proxy_password
             FROM settings_telegram WHERE id=0"""
    cursor.execute(sql)
    status, token, proxy, proxy_proto, proxy_server, proxy_port, \
        proxy_login, proxy_pass = cursor.fetchone()

    if status == 1:
        sql = "SELECT telegram_id FROM users WHERE alert_tg=1"
        cursor.execute(sql)
        # if not chats for sending message
        error_send_alert = ""
        try:
            chats = cursor.fetchall()[0]

            # Обновить состояние отправки на -1 (отправляется)
            sql = "UPDATE events SET alert_tg_status=-1 WHERE id_item=? AND hostname=?"
            cursor.execute(sql, (event_id, hostname))
            conn.commit()

            for chat_id in chats:
                status_send = send_message(token, chat_id, text, proxy, proxy_proto, proxy_server,
                                           proxy_port, proxy_login, proxy_pass)
                if status_send[0] is 0:
                    error_send_alert = error_send_alert + "%s<br>" % status_send[1]
            # если есть ошибки в отпрвке, то обновляем статус отправки
            if len(error_send_alert) != 0:
                sql = """UPDATE events SET alert_tg_status=0, alert_tg_desc=?
                         WHERE id_item=? AND hostname=?"""
                cursor.execute(sql, (error_send_alert, event_id, hostname))
                conn.commit()
            else:
                # Обновить состояние отправки на 1 (отправлено)
                sql = "UPDATE events SET alert_tg_status=1 WHERE id_item=? AND hostname=?"
                cursor.execute(sql, (event_id, hostname))
                conn.commit()

        except IndexError:
            sql = """UPDATE events SET alert_tg_status=0, alert_tg_desc=?
                     WHERE id_item=? AND hostname=?"""
            error_send_alert = "Нет адресов для отправки"
            cursor.execute(sql, (error_send_alert, event_id, hostname))
            conn.commit()

    conn.close()
