#!/usr/bin/env python3
import os
import sqlite3
from datetime import datetime, timedelta

import psutil


DB_PATH = os.path.dirname(os.path.abspath(__file__)) + "/../data.db"


def create_data_graph(data, name, element, color, date_col, in_GB=False):
    data_list = []
    for item in data:
        dot = {}
        dot['x'] = item[date_col]
        if in_GB is True:
            dot['y'] = "%.1f" % (((item[element] / 1024) / 1024) / 1024)
        else:
            dot['y'] = item[element]
        data_list.append(dot.copy())

    dataset = {}
    dataset['label'] = name
    dataset['data'] = data_list
    dataset['backgroundColor'] = color
    dataset['borderColor'] = color
    dataset['borderWidth'] = 1
    dataset['fill'] = 'false'

    return dataset


def top():
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()

    boot_time = datetime.fromtimestamp(psutil.boot_time())
    up = datetime.today() - boot_time
    uptime = "{days} days, {hours}h {seconds}m".format(days=up.days,
                                                       hours=(up.seconds // 3600),
                                                       seconds=(up.seconds % 3600 // 60))

    sql = "SELECT load_percent FROM cpu ORDER BY datetime DESC LIMIT 1"
    cursor.execute(sql)
    cpu_load = cursor.fetchone()[0]

    sql = "SELECT percent FROM memory ORDER BY datetime DESC LIMIT 1"
    cursor.execute(sql)
    mem_load = cursor.fetchone()[0]

    sql = "SELECT total,datetime FROM connections ORDER BY datetime DESC LIMIT 1"
    cursor.execute(sql)
    conn_total = cursor.fetchone()[0]

    load_average = os.getloadavg()

    response = {}
    response['uptime'] = uptime
    response['cpu_load'] = "{}%".format(cpu_load)
    response['mem_load'] = "{}%".format(mem_load)
    response['conn_total'] = conn_total
    response['load_average'] = "{0} {1} {2}".format(load_average[0], load_average[1],
                                                    load_average[2])

    conn.close()
    return response


def system_load(age):
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    if age == 'day' or age is None or age == 'null':
        yesterday = datetime.strftime((datetime.today() - timedelta(days=1)),
                                      "%Y-%m-%d %H:%M")
        sql = """SELECT user,system,nice,idle,iowait,steal,datetime
                 FROM cpu WHERE datetime BETWEEN ? AND ?"""
        cursor.execute(sql, (yesterday, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")))
        data = cursor.fetchall()
        cpu_info = []
        for item in data:
            if ":00" in item[6]:
                cpu_info.append(item)
        cpu_info.append(data[-1])

    if age == 'hour':
        hour_ago = datetime.strftime((datetime.today() - timedelta(minutes=60)),
                                     "%Y-%m-%d %H:%M")
        sql = """SELECT user,system,nice,idle,iowait,steal,datetime
                 FROM cpu WHERE datetime BETWEEN ? AND ?"""
        cursor.execute(sql, (hour_ago, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")))
        cpu_info = cursor.fetchall()

    if age == 'week':
        week_ago = datetime.strftime((datetime.today() - timedelta(days=7)),
                                     "%Y-%m-%d %H:%M")
        sql = """SELECT user,system,nice,idle,iowait,steal,datetime
                 FROM cpu WHERE datetime BETWEEN ? AND ?"""
        cursor.execute(sql, (week_ago, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")))
        data = cursor.fetchall()
        cpu_info = []
        for item in data:
            if "00:00" in item[6] or "06:00" in item[6] or \
               "12:00" in item[6] or "18:00" in item[6]:
                cpu_info.append(item)
        cpu_info.append(data[-1])

    user = create_data_graph(cpu_info, 'user', 0, 'rgba(255, 99, 132, 1)', 6)
    system = create_data_graph(cpu_info, 'system', 1, 'rgba(54, 162, 235, 1)', 6)
    # nice = create_data_graph(cpu_info, 'nice', 2, 'rgba(255, 206, 86, 1)', 6)
    idle = create_data_graph(cpu_info, 'idle', 3, 'rgba(75, 192, 192, 1)', 6)
    io_wait = create_data_graph(cpu_info, 'io_wait', 4, 'rgba(153, 102, 255, 1)', 6)
    steal = create_data_graph(cpu_info, 'steal', 5, 'rgba(255, 159, 64, 1)', 6)

    response = {}
    response['labels'] = [item[6] for item in cpu_info]

    datasets = []
    datasets.append(user.copy())
    datasets.append(system.copy())
    # datasets.append(nice.copy())
    datasets.append(idle.copy())
    datasets.append(io_wait.copy())
    datasets.append(steal.copy())
    response['datasets'] = datasets

    conn.close()
    return response


def mem_load():
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    sql = "SELECT used,free,buffers,cached FROM memory ORDER BY datetime DESC LIMIT 1"
    cursor.execute(sql)
    mem_load = cursor.fetchone()
    mem_used = "%.1f" % (((mem_load[0] / 1024) / 1024) / 1024)
    mem_free = "%.1f" % (((mem_load[1] / 1024) / 1024) / 1024)
    mem_buffers = "%.1f" % (((mem_load[2] / 1024) / 1024) / 1024)
    mem_cached = "%.1f" % (((mem_load[3] / 1024) / 1024) / 1024)

    datasets = []
    dot = {}
    dot['data'] = [mem_used, mem_free, mem_buffers, mem_cached]
    dot['backgroundColor'] = ['rgba(153, 102, 255, 1)',
                              'rgba(75, 192, 192, 1)',
                              'rgba(54, 162, 235, 1)',
                              'rgba(255, 159, 64, 1)']
    datasets.append(dot.copy())

    response = {}
    response['labels'] = ["used",
                          "free",
                          "buffers",
                          "cached"]
    response['datasets'] = datasets

    conn.close()
    return response


def swap_load():
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    sql = "SELECT used,free FROM swap ORDER BY datetime DESC LIMIT 1"
    cursor.execute(sql)
    swap_load = cursor.fetchone()
    swap_used = "%.1f" % (((swap_load[0] / 1024) / 1024) / 1024)
    swap_free = "%.1f" % (((swap_load[1] / 1024) / 1024) / 1024)

    datasets = []
    dot = {}
    dot['data'] = [swap_used, swap_free]
    dot['backgroundColor'] = ['rgba(255, 159, 64, 1)', 'rgba(75, 192, 192, 1)']
    datasets.append(dot.copy())

    response = {}
    response['labels'] = ["used", "free"]
    response['datasets'] = datasets

    conn.close()
    return response


def memory_usage(age):
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    if age == 'day' or age is None or age == 'null':
        yesterday = datetime.strftime((datetime.today() - timedelta(days=1)),
                                      "%Y-%m-%d %H:%M")
        sql = """SELECT used,free,available,buffers,cached,shared,datetime
                 FROM memory WHERE datetime BETWEEN ? AND ?"""
        cursor.execute(sql, (yesterday, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")))
        data = cursor.fetchall()
        mem_info = []
        for item in data:
            if ":00" in item[6]:
                mem_info.append(item)
        mem_info.append(data[-1])

    if age == 'hour':
        hour_ago = datetime.strftime((datetime.today() - timedelta(minutes=60)),
                                     "%Y-%m-%d %H:%M")
        sql = """SELECT used,free,available,buffers,cached,shared,datetime
                 FROM memory WHERE datetime BETWEEN ? AND ?"""
        cursor.execute(sql, (hour_ago, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")))
        mem_info = cursor.fetchall()

    if age == 'week':
        week_ago = datetime.strftime((datetime.today() - timedelta(days=7)),
                                     "%Y-%m-%d %H:%M")
        sql = """SELECT used,free,available,buffers,cached,shared,datetime
                 FROM memory WHERE datetime BETWEEN ? AND ?"""
        cursor.execute(sql, (week_ago, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")))
        data = cursor.fetchall()
        mem_info = []
        for item in data:
            if "00:00" in item[6] or "06:00" in item[6] or \
               "12:00" in item[6] or "18:00" in item[6]:
                mem_info.append(item)
        mem_info.append(data[-1])

    used = create_data_graph(mem_info, 'used', 0,
                             'rgba(255, 99, 132, 1)', 6, in_GB=True)
    free = create_data_graph(mem_info, 'free', 1,
                             'rgba(54, 162, 235, 1)', 6, in_GB=True)
    shared = create_data_graph(mem_info, 'shared', 2,
                               'rgba(255, 206, 86, 1)', 6, in_GB=True)
    available = create_data_graph(mem_info, 'available', 3,
                                  'rgba(75, 192, 192, 1)', 6, in_GB=True)
    buffers = create_data_graph(mem_info, 'buffers', 4,
                                'rgba(153, 102, 255, 1)', 6, in_GB=True)
    cached = create_data_graph(mem_info, 'cached', 5,
                               'rgba(255, 159, 64, 1)', 6, in_GB=True)

    response = {}
    response['labels'] = [item[6] for item in mem_info]

    datasets = []
    datasets.append(used.copy())
    datasets.append(free.copy())
    datasets.append(shared.copy())
    datasets.append(available.copy())
    datasets.append(buffers.copy())
    datasets.append(cached.copy())
    response['datasets'] = datasets

    conn.close()
    return response


def disks(device, data_type=None, age=None):
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    if device == 'all':
        # get last row datetime in table
        sql = "SELECT datetime FROM disks ORDER BY datetime DESC LIMIT 1"
        cursor.execute(sql)

        sql = "SELECT DISTINCT device FROM disks WHERE datetime=?"
        cursor.execute(sql, cursor.fetchone())
        disks = []
        for item in cursor.fetchall():
            disks.append(item[0].replace("/", "_"))

        conn.close()
        return disks

    else:
        if data_type == 'percent':
            device = device.replace("_", "/")
            sql = """SELECT device,mountpoint,percent,datetime
                     FROM disks WHERE device=? ORDER BY datetime DESC LIMIT 1"""
            cursor.execute(sql, (device,))
            disk_info = cursor.fetchone()

            response = []

            datasets = []
            dot = {}
            dot['data'] = [disk_info[2], "%.1f" % (100 - disk_info[2])]
            dot['backgroundColor'] = ['rgba(255, 99, 132, 1)', 'rgba(75, 192, 192, 1)']
            datasets.append(dot.copy())

            disk = []
            disk_graph = {}
            disk_graph['labels'] = ["used", "free"]
            disk_graph['datasets'] = datasets

            disk.append(disk_graph.copy())
            disk.append(disk_info[1])
            disk.append(disk_info[0])

            response.append(disk)
            conn.close()
            return response

        if data_type == 'volume':
            device = device.replace("_", "/")
            sql = """SELECT device,mountpoint,used,free,datetime
                     FROM disks WHERE device=? ORDER BY datetime DESC LIMIT 1"""
            cursor.execute(sql, (device,))
            disk_info = cursor.fetchone()

            response = []

            datasets = []
            dot = {}
            dot['data'] = ["%.2f" % (((disk_info[2] / 1024) / 1024) / 1024),
                           "%.2f" % (((disk_info[3] / 1024) / 1024) / 1024)]
            dot['backgroundColor'] = ['rgba(153, 102, 255, 1)', 'rgba(75, 192, 192, 1)']
            datasets.append(dot.copy())

            disk = []
            disk_graph = {}
            disk_graph['labels'] = ["used", "free"]
            disk_graph['datasets'] = datasets

            disk.append(disk_graph.copy())
            disk.append(disk_info[1])
            disk.append(disk_info[0])

            response.append(disk)
            conn.close()
            return response

        if data_type == 'graph':
            device = device.replace("_", "/")
            if age == 'day' or age is None or age == 'null':
                yesterday = datetime.strftime((datetime.today() - timedelta(days=1)),
                                              "%Y-%m-%d %H:%M")
                sql = """SELECT device,mountpoint,total,used,free,datetime
                         FROM disks WHERE datetime BETWEEN ? AND ? AND device=?"""
                cursor.execute(sql, (yesterday, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M"),
                                     device))
                data = cursor.fetchall()
                disk_info = []
                for item in data:
                    if ":00" in item[5]:
                        disk_info.append(item)
                disk_info.append(data[-1])

            if age == 'hour':
                hour_ago = datetime.strftime((datetime.today() - timedelta(minutes=60)),
                                             "%Y-%m-%d %H:%M")
                sql = """SELECT device,mountpoint,total,used,free,datetime
                         FROM disks WHERE datetime BETWEEN ? AND ? AND device=?"""
                cursor.execute(sql, (hour_ago, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M"),
                                     device))
                disk_info = cursor.fetchall()

            if age == 'week':
                week_ago = datetime.strftime((datetime.today() - timedelta(days=7)),
                                             "%Y-%m-%d %H:%M")
                sql = """SELECT device,mountpoint,total,used,free,datetime
                         FROM disks
                         WHERE datetime BETWEEN ? AND ? AND device=?"""
                cursor.execute(sql, (week_ago, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M"),
                                     device))
                data = cursor.fetchall()
                disk_info = []
                for item in data:
                    if "00:00" in item[5] or "06:00" in item[5] or \
                       "12:00" in item[5] or "18:00" in item[5]:
                        disk_info.append(item)
                disk_info.append(data[-1])

            total = create_data_graph(disk_info, 'total', 2,
                                      'rgba(255, 159, 64, 0.4)', 5, in_GB=True)
            used = create_data_graph(disk_info, 'used', 3,
                                     'rgba(255, 99, 132, 1)', 5, in_GB=True)
            free = create_data_graph(disk_info, 'free', 4,
                                     'rgba(75, 192, 192, 1)', 5, in_GB=True)

            response = []

            disk_graph = {}
            disk_graph['labels'] = [item[5] for item in disk_info]
            datasets = []
            datasets.append(total.copy())
            datasets.append(used.copy())
            datasets.append(free.copy())
            disk_graph['datasets'] = datasets

            disk = []

            disk.append(disk_graph.copy())
            disk.append(disk_info[0][1])
            disk.append(disk_info[0][0])

            response.append(disk)

            conn.close()
            return response
