#!/usr/bin/env python3
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sqlite3
import os
import smtplib


DB_PATH = os.path.dirname(os.path.abspath(__file__)) + "/../data.db"


def send_email(server, port, password, subject,
               emails, from_addr, body_text):
    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = ', '.join(emails)
    msg['Subject'] = subject

    body = body_text
    msg.attach(MIMEText(body, 'plain'))

    smtp = smtplib.SMTP(server, port)
    smtp.starttls()
    try:
        smtp.login(from_addr, password)
    except:
        return 0, "Authentication failed: Invalid user or password"
    smtp.send_message(msg)
    smtp.quit()


def run(event_id, hostname, subject, body_text):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    sql = "SELECT status, server, port, login, password FROM settings_smtp WHERE id=0"
    cursor.execute(sql)
    status, server, port, from_addr, password = cursor.fetchone()

    if status == 1:
        sql = "SELECT email FROM users WHERE alert_email=1"
        cursor.execute(sql)
        # if not email for sending message
        try:
            emails = cursor.fetchall()[0]
            # Обновить состояние отправки на -1 (отправляется)
            sql = "UPDATE events SET alert_email_status=-1 WHERE id_item=? AND hostname=?"
            cursor.execute(sql, (event_id, hostname))
            conn.commit()

            status_send = send_email(server, port, password, subject,
                                     emails, from_addr, body_text)
            if status_send is None:
                sql = "UPDATE events SET alert_email_status=1 WHERE id_item=? AND hostname=?"
                cursor.execute(sql, (event_id, hostname))
                conn.commit()
            elif status_send[0] == 0:
                sql = """UPDATE events SET alert_email_status=0, alert_email_desc=?
                         WHERE id_item=? AND hostname=?"""
                cursor.execute(sql, (status_send[1], event_id, hostname))
                conn.commit()

        except IndexError:
            sql = """UPDATE events SET alert_email_status=0, alert_email_desc=?
                     WHERE id_item=? AND hostname=?"""
            error_send_alert = "Нет адресов для отправки"
            cursor.execute(sql, (error_send_alert, event_id, hostname))
            conn.commit()

    conn.close()
