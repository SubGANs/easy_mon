-- CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE uptime(
 id int default 0,
 datetime int not null
);
INSERT INTO uptime(id, datetime) VALUES (0, 0);
CREATE TABLE cpu(
 count_processors int not null,
 load_percent int not null,
 user real not null,
 system real not null,
 nice real not null,
 idle real not null,
 iowait real not null,
 irq real not null,
 softirq real not null,
 steal real not null,
 frequency real not null,
 count_precesses int not null,
 datetime int not null
 );
CREATE TABLE swap(
 total int not null,
 used int not null,
 free int not null,
 percent real not null,
 datetime int not null
);
CREATE TABLE memory(
 total int not null,
 available int not null,
 percent real not null,
 used int not null,
 free int not null,
 active int not null,
 inactive int not null,
 buffers int not null,
 cached int not null,
 shared int not null,
 slab int not null,
 datetime int not null
);
CREATE TABLE disks(
 device real not null,
 mountpoint real not null,
 fstype real not null,
 opts real not null,
 total int not null,
 used int not null,
 free int not null,
 percent real not null,
 datetime int not null
);
CREATE TABLE connections(
 total int not null,
 datetime int not null
);
CREATE TABLE users(
 user text not null unique,
 password text not null,
 level int not null,
 email text default "",
 telegram_id text default "",
 alert_email int default 0,
 alert_tg int default 0
);
INSERT INTO users (user, password, level) VALUES ('admin', '$pbkdf2-sha256$30000$FMIYQ.gdAyAEwJhTau095w$HNqPEAnbn.NdO8cALcX/EkVBa7SU6TMAzdU0eoiyhC0', 0);
CREATE TABLE settings_smtp(
 id int default 0,
 status int default 0,
 server text default "",
 port int default "",
 login text default "",
 password text default ""
);
INSERT INTO settings_smtp (id) VALUES (0);
CREATE TABLE settings_telegram(
 id int default 0,
 status int default 0,
 token text default "",
 proxy_status int default 0,
 proxy_proto text default "",
 proxy_server text default "",
 proxy_port int default "",
 proxy_login text default "",
 proxy_password text default ""
);
INSERT INTO settings_telegram (id) VALUES (0);
CREATE TABLE events(
 datetime int not null,
 id_item text not null,
 hostname text default "",
 description text default "",
 alert_tg_status int default 0,
 alert_tg_desc text default "",
 alert_email_status int default 0,
 alert_email_desc text default ""
);
CREATE TABLE services(
 name text not null unique,
 status int default 0,
 reboot int default 1
);
