Install EasyMon.

1. Clone repo: git clone
2. Install virtual env for python: python3 -m venv venv
3. Activate venv: source venv/bin/activate
4. Install requirements: pip install -r requirements.txt
5. Create DB: sqlite3 data.db < schema.sql
6. Run scripts:
	* python scheduler.py
	* python app.py
7. Open in browser: 127.0.0.1:8888
