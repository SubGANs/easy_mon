#!/bin/bash

if [[ `df -i | grep -w "/" | awk '{print $5}' | sed -s 's/%//g'` > 90 ]]; then
  echo "Free innode is less than 10% on volume /"
fi
