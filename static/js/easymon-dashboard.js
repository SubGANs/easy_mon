// Graph System Load
var ctx = document.getElementById("chart_SystemLoad").getContext("2d");
var chart_SystemLoad = new Chart(ctx, { type: 'line' });
function systemLoad(update) {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/_get?stat=system_load&age=" + $.cookie('system_load_age'),
    async: true,
    success: function(data){
      chart_SystemLoad.data.labels = data['labels']
      chart_SystemLoad.data.datasets = data['datasets'];
      chart_SystemLoad.update(duration=update);
    }
  });
}

// Set default cookie and add icon on choose settings system load
if ($.cookie('system_load_timeout') == null){
  $.cookie('system_load_timeout', 60000)
  $(`a#settings[data-id='system_load_${$.cookie('system_load_timeout')}']`).append('<span id="system_load_timeout" class="uk-margin-small-right" uk-icon="check"></span>')
} else {
  var value = $.cookie('system_load_timeout')
  $(`a#settings[data-id='system_load_${value}']`).append('<span id="system_load_timeout" class="uk-margin-small-right" uk-icon="check"></span>')
}

if ($.cookie('system_load_age') == null){
  $.cookie('system_load_age', "day")
  $(`a#settings[data-id='system_load_${$.cookie('system_load_age')}']`).append('<span id="system_load_age" class="uk-margin-small-right" uk-icon="check"></span>')
} else {
  var value = $.cookie('system_load_age')
  $(`a#settings[data-id='system_load_${value}']`).append('<span id="system_load_age" class="uk-margin-small-right" uk-icon="check"></span>')
}

// Auto update system load graph
window.setInterval(function(){
  systemLoad(update=0);
}, $.cookie('system_load_timeout'));

systemLoad();



// Memory Usage Graph
var ctx = document.getElementById("chart_MemoryUsage").getContext("2d");
var chart_MemoryUsage = new Chart(ctx, { type: 'line' });
function memoryUsage(update) {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/_get?stat=memory_usage&age=" + $.cookie('memory_usage_age'),
    async: true,
    success: function(data){
      chart_MemoryUsage.data.labels = data['labels']
      chart_MemoryUsage.data.datasets = data['datasets'];
      chart_MemoryUsage.update(duration=update);
    }
  });
}

// Set default cookie and add icon on choose settings system load
if ($.cookie('memory_usage_timeout') == null){
  $.cookie('memory_usage_timeout', 60000)
  $(`a#settings[data-id='memory_usage_${$.cookie('memory_usage_timeout')}']`).append('<span id="memory_usage_timeout" class="uk-margin-small-right" uk-icon="check"></span>')
} else {
  var value = $.cookie('memory_usage_timeout')
  $(`a#settings[data-id='memory_usage_${value}']`).append('<span id="memory_usage_timeout" class="uk-margin-small-right" uk-icon="check"></span>')
}

if ($.cookie('memory_usage_age') == null){
  $.cookie('memory_usage_age', "day")
  $(`a#settings[data-id='memory_usage_${$.cookie('memory_usage_age')}']`).append('<span id="memory_usage_age" class="uk-margin-small-right" uk-icon="check"></span>')
} else {
  var value = $.cookie('memory_usage_age')
  $(`a#settings[data-id='memory_usage_${value}']`).append('<span id="memory_usage_age" class="uk-margin-small-right" uk-icon="check"></span>')
}

// Auto update system load graph
window.setInterval(function(){
  var update = true;
  memoryUsage(update=0);
}, $.cookie('memory_usage_timeout'));

memoryUsage();


// Graph Memory Load
var ctx = document.getElementById("chart_MemoryLoad").getContext("2d");
var chart_MemoryLoad = new Chart(ctx, { type: 'doughnut' });
function memLoad(update) {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/_get?stat=mem_load",
    async: true,
    success: function(data){
      chart_MemoryLoad.data.labels = data['labels']
      chart_MemoryLoad.data.datasets = data['datasets'];
      chart_MemoryLoad.update(duration=update);
    }
  });
}
memLoad();

// Auto update memory load
window.setInterval(function(){
  memLoad(update=0);
}, 60000);

// Graph Swap Load
var ctx = document.getElementById("chart_SwapLoad").getContext("2d");
var chart_SwapLoad = new Chart(ctx, { type: 'doughnut' });
function swapLoad(update) {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/_get?stat=swap_load",
    async: true,
    success: function(data){
      chart_SwapLoad.data.labels = data['labels']
      chart_SwapLoad.data.datasets = data['datasets'];
      chart_SwapLoad.update(duration=update);
    }
  });
}
swapLoad();

// Auto update swap load
window.setInterval(function(){
  swapLoad(update=0);
}, 60000);


// Disks usage

function update_disks_chart(device, data_type, graph_type="doughnut") {
  var ctx = document.getElementById(`chart_DiskUsage_${data_type}_${device}`).getContext("2d");
  var chart_DiskUsage = new Chart(ctx, { type: graph_type });
  var age = $.cookie(`disk_usage_age_${device}`)
  $.ajax({
    type: "GET",
    dataType: "json",
    url: `/_get?stat=disks&data_type=${data_type}&device=${device}&age=${age}`,
    async: true,
    success: function(data){
      data.forEach(function(item, i, data) {
        chart_DiskUsage.data.labels = data[0][0]['labels'];
        chart_DiskUsage.data.datasets = data[0][0]['datasets'];
        chart_DiskUsage.update(duration=0);
        $(`#disk_name_${device}_${data_type}`).text(data[0][1])
        $(`#disk_mountpoint_${device}_${data_type}`).text(data[0][2])
      });
    }
  });
};

function add_disks_chart() {
  $.ajax({
        type: "GET",
        dataType: "json",
        url: "/_get?stat=disks&device=all",
        async: true,
        success: function(data) {
          data.forEach(function(item, i, data) {
            var disk_name = item.toString()
            update_disks_chart(disk_name, data_type="graph", graph_type="line")
            update_disks_chart(disk_name, data_type="percent")
            update_disks_chart(disk_name, data_type="volume")
          });
        }
      });
};

add_disks_chart();

// System Load in top page
function topInfo(update) {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "/_get?stat=top",
    async: true,
    success: function(data){
      $("h1#uptime").text(data['uptime'])
      $("h1#cpu_load").text(data['cpu_load'])
      $("h1#mem_load").text(data['mem_load'])
      $("h1#conn_total").text(data['conn_total'])
      $("h1#load_average").text(data['load_average'])
    }
  });
}

window.setInterval(function(){
  topInfo();
}, 60000);

topInfo();



// Update icon on choose settings
$("a#settings").click(function(){
  if ($(this).data("timeout")) {
    var name = $(this).data("name");
    var timeout = $(this).data("timeout");
    $.cookie(name, timeout)
    $("span#" + name).remove()
    $(this).append(`<span id="${name}"  class="uk-margin-small-right" uk-icon="check"></span>`)
  }
  if ($(this).data("age")) {
    var name = $(this).data("name");
    var age = $(this).data("age");
    $.cookie(name, age)
    $("span#" + name).remove()
    $(this).append(`<span id="${name}"  class="uk-margin-small-right" uk-icon="check"></span>`)
  }
  })