#!/usr/bin/env python3
from datetime import datetime, timedelta
import sqlite3
import os
import platform
from subprocess import Popen, PIPE

import psutil
import tornado.ioloop

from utils import sendmail
from utils import telegram


DB_PATH = os.path.dirname(os.path.abspath(__file__)) + "/data.db"


def bytes2human(n):
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n


def get_system_info():
    """ Планировщик, раз в минуту собирает статистику с сервера и заносит в БД."""
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()

    time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")

    # UPTIME
    sql = "UPDATE uptime SET datetime=? WHERE id=0"
    cursor.execute(sql, (psutil.boot_time(),))
    conn.commit()

    # CPU
    cpu_count = psutil.cpu_count()
    cpu_load_percent = psutil.cpu_percent()

    cpu_times_percent = psutil.cpu_times_percent()
    cpu_us = cpu_times_percent[0]
    cpu_sy = cpu_times_percent[2]
    cpu_ni = cpu_times_percent[1]
    cpu_id = cpu_times_percent[3]
    cpu_wa = cpu_times_percent[4]
    cpu_hi = cpu_times_percent[5]
    cpu_si = cpu_times_percent[6]
    cpu_st = cpu_times_percent[7]

    try:
        cpu_freq = psutil.cpu_freq()[0]
    except TypeError:
        cpu_freq = 0
    cpu_count_processes = len(psutil.pids())

    sql = """INSERT INTO cpu
                (
                    count_processors, load_percent, user, system, nice, idle,
                    iowait, irq, softirq, steal, frequency, count_precesses, datetime
                )
             VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"""
    cursor.execute(sql, (cpu_count, cpu_load_percent, cpu_us, cpu_sy, cpu_ni, cpu_id,
                         cpu_wa, cpu_hi, cpu_si, cpu_st, cpu_freq, cpu_count_processes, time))
    conn.commit()

    # MEMORY
    mem = psutil.virtual_memory()
    mem_total = mem[0]
    mem_available = mem[1]
    mem_percent = mem[2]
    mem_used = mem[3]
    mem_free = mem[4]
    mem_active = mem[5]
    mem_inactive = mem[6]
    mem_buffers = mem[7]
    mem_cached = mem[8]
    mem_shared = mem[9]
    mem_slab = mem[10]
    sql = """INSERT INTO memory
                (
                    total, available, percent, used, free, active,
                    inactive, buffers, cached, shared, slab, datetime
                )
             VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"""
    cursor.execute(sql, (mem_total, mem_available, mem_percent, mem_used, mem_free, mem_active,
                         mem_inactive, mem_buffers, mem_cached, mem_shared, mem_slab, time))
    conn.commit()

    # SWAP
    swap = psutil.swap_memory()
    swap_total = swap[0]
    swap_used = swap[1]
    swap_free = swap[2]
    swap_percent = swap[3]
    sql = """INSERT INTO swap
                (
                    total, used, free, percent, datetime
                )
             VALUES (?,?,?,?,?)"""
    cursor.execute(sql, (swap_total, swap_used, swap_free, swap_percent, time))
    conn.commit()

    # DISK
    partitions = psutil.disk_partitions()
    for item in partitions:
        disk_device = item[0]
        disk_mountpoint = item[1]
        disk_fstype = item[2]
        disk_opts = item[3]

        disk_usage = psutil.disk_usage(item[1])
        disk_total = disk_usage[0]
        disk_used = disk_usage[1]
        disk_free = disk_usage[2]
        disk_percent = disk_usage[3]

        sql = """INSERT INTO disks
                    (
                        device, mountpoint, fstype, opts, total, used, free, percent, datetime
                    )
                 VALUES (?,?,?,?,?,?,?,?,?)"""
        cursor.execute(sql, (disk_device, disk_mountpoint, disk_fstype, disk_opts,
                             disk_total, disk_used, disk_free, disk_percent, time))
        conn.commit()

    # NETWORKS
    net = psutil.net_connections()
    net_total = len(net)
    sql = "INSERT INTO connections(total, datetime) VALUES (?,?)"
    cursor.execute(sql, (net_total, time))
    conn.commit()


def delete_old_event(problems):
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()

    hostname = platform.uname()[1]
    SUBJECT_MESSAGE = "[EasyMon] Resolved problem on '{0}'".format(hostname)
    time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")

    sql = "SELECT id_item, datetime, description FROM events WHERE hostname=?"
    cursor.execute(sql, (hostname,))
    events = cursor.fetchall()

    for event in events:
        i = 0
        for problem in problems:
            if event[0] == problem[0]:
                continue
            else:
                i += 1
        if i == len(problems):
            sql = "DELETE FROM events WHERE id_item=? AND hostname=?"
            cursor.execute(sql, (event[0], hostname))
            conn.commit()

            telegram.run(event[0], hostname,
                         "<b>%s</b>\n%s %s" % (SUBJECT_MESSAGE, time, event[2]))
            sendmail.run(event[0], hostname, SUBJECT_MESSAGE, event[2])

    conn.close()


def check_triger():
    """Раз в минуту проверяет состояние триггеров
    и запускает задание на отправку уведомлений."""

    PROBLEMS = []  # list with problems
    time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")
    hostname = platform.uname()[1]

    # Check uptime
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    sql = "SELECT datetime FROM uptime WHERE id=0"
    cursor.execute(sql)
    if psutil.boot_time() > cursor.fetchone()[0]:
        PROBLEMS.append(("reboot", hostname, "Выполнена перезагрузка"))

    # Check LA
    load_average = os.getloadavg()
    if load_average[0] >= psutil.cpu_count():
        PROBLEMS.append(("la", hostname, "Высокий LA {0} {1} {2}.".format(
            load_average[0], load_average[1], load_average[2])))

    # Check mem used
    mem_util_percent = psutil.virtual_memory()[2]
    if mem_util_percent > 90:
        PROBLEMS.append(("mem", hostname,
                         """Высокий уровень потребления оперативной памяти - {}. Свободно меньше 10%.""".format(mem_util_percent)))

    # Check swap used
    swap_util_percent = psutil.swap_memory()[3]
    if swap_util_percent > 90:
        PROBLEMS.append(("swap", hostname,
                         """Высокий уровень потребления swap - {}. Свободно меньше 10%.""".format(swap_util_percent)))

    # Check disks used
    partitions = psutil.disk_partitions()
    for item in partitions:
        disk_used_percent = psutil.disk_usage(item[1])[3]

        if disk_used_percent > 90:
            PROBLEMS.append(("disk_%s" % item[1], hostname,
                             """Свободного места на {0} [{1}] меньше 10%.""".format(
                             item[1], item[0])))

    # Check servces
    sql = "SELECT name,reboot from services"
    cursor.execute(sql)
    for service in cursor.fetchall():
        cmd = '/bin/systemctl status %s' % service[0]
        proc = Popen(cmd, shell=True, stdout=PIPE)
        if '(running)' in proc.communicate()[0].decode("utf-8"):
            sql = "UPDATE services SET status=1 WHERE name=?"
            cursor.execute(sql, (service[0],))
            conn.commit()
        else:
            if service[1] == 1:
                cmd = '/bin/systemctl restart %s' % service[0]
                proc = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
                if '(running)' in proc.communicate()[0].decode("utf-8"):
                    PROBLEMS.append((service[0], hostname,
                                     "Служба %s была перезагружена" % service[0]))
                    sql = "UPDATE services SET status=1 WHERE name=?"
                    cursor.execute(sql, (service[0],))
                    conn.commit()
                else:
                    PROBLEMS.append((service[0], hostname,
                                     "Служба %s остановлена (перезагрузка не удалась)" % service[0]))
                    sql = "UPDATE services SET status=0 WHERE name=?"
                    cursor.execute(sql, (service[0],))
                    conn.commit()
            else:
                PROBLEMS.append((service[0], hostname,
                                 "Служба %s остановлена" % service[0]))
                sql = "UPDATE services SET status=0 WHERE name=?"
                cursor.execute(sql, (service[0],))
                conn.commit()

    # USERS TRIGGERS
    commands_dir = os.path.dirname(os.path.abspath(__file__)) + "/commands/"
    files = [f for f in os.listdir(commands_dir) if os.path.isfile(os.path.join(commands_dir, f))]
    for file in files:
        if file.startswith("."):
            continue
        call_script = Popen(commands_dir + file, shell=True, stdout=PIPE, stderr=PIPE)
        call_script.wait()
        output, error = call_script.communicate()
        if len(output) > 0:
            PROBLEMS.append((file, hostname, output.decode("utf-8")))

    SUBJECT_MESSAGE = "[EasyMon] New problem on '{0}'".format(hostname)
    for problem in PROBLEMS:
        conn = sqlite3.connect(DB_PATH, timeout=10)
        cursor = conn.cursor()

        event_id = problem[0]
        event_text = problem[2]

        # check exists event
        sql = """SELECT hostname, id_item, alert_tg_status, alert_email_status
                 FROM events WHERE id_item=? AND hostname=?"""
        cursor.execute(sql, (event_id, hostname))

        event = cursor.fetchone()
        # Если event уже есть
        if event is not None:
            # Если оповещение в ТГ не отправилось, то отправляем повторно
            if event[2] is 0:
                telegram.run(event_id, hostname,
                             "<b>%s</b>\n%s %s" % (SUBJECT_MESSAGE, time, event_text))
            # Если email не отправлось, то отправляем повторно
            if event[3] is 0:
                sendmail.run(event_id, hostname, SUBJECT_MESSAGE, event_text)
        else:
            # Если event не создан, то создаем и отправляем письмо
            sql = "INSERT INTO events(datetime, id_item, hostname, description) VALUES (?,?,?,?)"
            cursor.execute(sql, (time, event_id, hostname, event_text))
            conn.commit()

            sendmail.run(event_id, hostname, SUBJECT_MESSAGE, event_text)

            telegram.run(event_id, hostname,
                         "<b>%s</b>\n%s %s" % (SUBJECT_MESSAGE, time, event_text))

        conn.close()

    delete_old_event(PROBLEMS)


def delete_old_item():
    """Удаление старых значений из БД"""
    month_ago = datetime.strftime((datetime.today() - timedelta(days=31)),
                                  "%Y-%m-%d %H:%M")
    conn = sqlite3.connect(DB_PATH, timeout=10)
    cursor = conn.cursor()
    tables = (
        "cpu",
        "connections",
        "disks",
        "memory",
        "swap"
    )
    for table in tables:
        sql = "DELETE FROM %s WHERE datetime<?" % table
        cursor.execute(sql, (month_ago,))
        conn.commit()
    conn.close()


if __name__ == "__main__":
    interval_sec = 60 * 1000  # 1 min
    main_loop = tornado.ioloop.IOLoop.instance()

    # check trigger scheduler
    trigger = tornado.ioloop.PeriodicCallback(check_triger, interval_sec)
    trigger.start()

    # get system info scheduler
    sched = tornado.ioloop.PeriodicCallback(get_system_info, interval_sec)
    sched.start()

    # delete old items from DB
    cleaner = tornado.ioloop.PeriodicCallback(delete_old_item, interval_sec)
    cleaner.start()

    main_loop.start()
