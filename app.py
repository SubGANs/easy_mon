#!/usr/bin/env python3
import os
import json
import platform
import sqlite3
import re
import socket

import psutil
import cpuinfo

import tornado.ioloop
import tornado.web
import tornado.auth
import tornado.escape
from tornado.options import define, options, parse_config_file

from utils import sys_info, ifconfig

from passlib.context import CryptContext

DB_PATH = os.path.dirname(os.path.abspath(__file__)) + "/data.db"
pwd_context = CryptContext(schemes=["pbkdf2_sha256"],
                           default="pbkdf2_sha256",
                           pbkdf2_sha256__default_rounds=30000)


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        try:
            user = self.get_secure_cookie("user").decode("utf-8").replace('"', "")
        except AttributeError:
            user = None
        return user


class MainHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        sys = platform.uname()
        kernel = sys[0]
        hostname = sys[1]
        kernel_release = sys[2]
        kernel_version = sys[3]
        arch = platform.machine()
        cpu_brand = cpuinfo.get_cpu_info()['brand']
        mem_total = "%.1fG" % (((psutil.virtual_memory()[0] / 1024) / 1024) / 1024)
        swap_total = "%.1fG" % (((psutil.swap_memory()[0] / 1024) / 1024) / 1024)

        disks = sys_info.disks(device="all")

        self.render("dashboard.html", tab_active='dashboard', hostname=hostname,
                    kernel=kernel, kernel_release=kernel_release,
                    kernel_version=kernel_version, arch=arch,
                    cpu_brand=cpu_brand, mem_total=mem_total,
                    swap_total=swap_total, disks=disks)


class LoginHandler(BaseHandler):
    def get(self):
        self.render("login.html", next=self.get_argument("next", "/"), login_error=False)

    def check_permission(self, password, username):
        conn = sqlite3.connect(DB_PATH, timeout=10)
        cursor = conn.cursor()
        sql = "SELECT password FROM users WHERE user=?"
        cursor.execute(sql, (username,))
        try:
            hash_pass = cursor.fetchone()[0]
        except TypeError:  # if user not found
            conn.close()
            return False
        if pwd_context.verify(password, hash_pass):
            conn.close()
            return True
        conn.close()
        return False

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "")
        auth = self.check_permission(password, username)
        if auth:
            self.set_current_user(username)
            self.redirect(self.get_argument("next", u"/"))
        else:
            self.render("login.html", next=self.get_argument("next", "/"), login_error=True)

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")


class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(u"/login")


class SettingsHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        conn = sqlite3.connect(DB_PATH, timeout=10)
        cursor = conn.cursor()

        # get user alert settings
        sql = "SELECT email, telegram_id, alert_email, alert_tg FROM users WHERE user=?"
        cursor.execute(sql, (self.current_user,))
        email, telegram_id, alert_email, alert_tg = cursor.fetchone()

        # get smtp settings
        sql = """SELECT status, server, port, login, password FROM settings_smtp
                 WHERE id=0"""
        cursor.execute(sql)
        smtp_status, smtp_server, smtp_port, smtp_login, smtp_password = cursor.fetchone()

        # get tg bot settings
        sql = """SELECT status, token, proxy_status, proxy_proto, proxy_server,
                proxy_port, proxy_login, proxy_password FROM settings_telegram
                WHERE id=0"""
        cursor.execute(sql)
        tg_status, tg_token, tg_proxy_status, tg_proxy_proto, tg_proxy_server, \
            tg_proxy_port, tg_proxy_login, tg_proxy_password = cursor.fetchone()

        # get users
        sql = "SELECT user,level FROM users"
        cursor.execute(sql)
        users = cursor.fetchall()

        # get user level
        sql = "SELECT level FROM users WHERE user=?"
        cursor.execute(sql, (self.current_user,))
        level = cursor.fetchone()[0]

        # get services
        sql = "SELECT name,status,reboot FROM services"
        cursor.execute(sql)
        services = cursor.fetchall()

        # check exist error message
        alert_msg = self.get_secure_cookie("alert_msg")
        alert_msg_status = self.get_secure_cookie("alert_msg_status")
        self.clear_cookie("alert_msg")
        self.clear_cookie("alert_msg_status")

        var = {
            "level": level,
            "alert_msg": alert_msg,
            "alert_msg_status": alert_msg_status,
            "email": email,
            "telegram_id": telegram_id,
            "alert_email": alert_email,
            "alert_tg": alert_tg,
            "users": users,
            "smtp_status": smtp_status,
            "smtp_server": smtp_server,
            "smtp_port": smtp_port,
            "smtp_login": smtp_login,
            "smtp_password": smtp_password,
            "tg_status": tg_status,
            "tg_token": tg_token,
            "tg_proxy_status": tg_proxy_status,
            "tg_proxy_proto": tg_proxy_proto,
            "tg_proxy_server": tg_proxy_server,
            "tg_proxy_port": tg_proxy_port,
            "tg_proxy_login": tg_proxy_login,
            "tg_proxy_password": tg_proxy_password,
            "services": services
        }

        self.render("settings.html", tab_active="dashboard", var=var)

    def post(self):
        if self.get_query_argument("change_password", default=None) == "":
            if self.get_argument("password1") != self.get_argument("password2"):
                self.set_secure_cookie("alert_msg", "Пароли не совпадают")
                self.set_secure_cookie("alert_msg_status", "danger")
                self.redirect(u'/settings')

            elif len(self.get_argument("password1")) <= 6:
                self.set_secure_cookie("alert_msg", "Пароль слишком короткий")
                self.set_secure_cookie("alert_msg_status", "danger")
                self.redirect(u"/settings")

            else:
                hash_pass = pwd_context.hash(self.get_argument("password1"))
                conn = sqlite3.connect(DB_PATH, timeout=10)
                cursor = conn.cursor()
                sql = "UPDATE users SET password=? WHERE user=?"
                cursor.execute(sql, (hash_pass, self.current_user))
                conn.commit()
                conn.close()
                self.set_secure_cookie("alert_msg", "Пароль успешно изменён")
                self.set_secure_cookie("alert_msg_status", "success")
                self.redirect(u"/settings")

        if self.get_query_argument("profile_alert", default=None) == "":
            email = self.get_argument("email")
            telegram_id = self.get_argument("telegram_id")
            alert_email = self.get_argument("alert_email", default=0)
            alert_tg = self.get_argument("alert_tg", default=0)

            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()
            # change user alert settings
            sql = "UPDATE users SET email=?, telegram_id=?, alert_email=?, alert_tg=? WHERE user=?"
            cursor.execute(sql, (email, telegram_id, alert_email, alert_tg, self.current_user))
            conn.commit()
            conn.close()

            self.set_secure_cookie("alert_msg", "Данные обновлены")
            self.set_secure_cookie("alert_msg_status", "success")
            self.redirect(u"/settings")

        if self.get_query_argument("add_user", default=None) == "":
            if self.get_argument("password1") != self.get_argument("password2"):
                self.set_secure_cookie("alert_msg", "Пароли не совпадают")
                self.set_secure_cookie("alert_msg_status", "danger")
                self.redirect(u'/settings')

            elif len(self.get_argument("password1")) <= 6:
                self.set_secure_cookie("alert_msg", "Пароль слишком короткий")
                self.set_secure_cookie("alert_msg_status", "danger")
                self.redirect(u"/settings")

            else:
                hash_pass = pwd_context.hash(self.get_argument("password1"))
                conn = sqlite3.connect(DB_PATH, timeout=10)
                cursor = conn.cursor()
                sql = "INSERT INTO users (user,password,level) VALUES (?,?,?)"
                try:
                    cursor.execute(sql, (self.get_argument("username"), hash_pass,
                                         self.get_argument("level")))
                except Exception as err:
                    if "UNIQUE constraint failed" in str(err):
                        self.set_secure_cookie("alert_msg",
                                               "Пользователь с таким именем уже существует")
                        self.set_secure_cookie("alert_msg_status", "danger")
                        self.redirect(u"/settings")
                    else:
                        self.set_secure_cookie("alert_msg", "Произошла внутренняя ошибка")
                        self.set_secure_cookie("alert_msg_status", "danger")
                        self.redirect(u"/settings")
                conn.commit()
                conn.close()
                self.set_secure_cookie("alert_msg", "Пользователь добавлен")
                self.set_secure_cookie("alert_msg_status", "success")
                self.redirect(u"/settings")

        if self.get_query_argument("change_user_level", default=None) == "":
            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()
            sql = "UPDATE users SET level=? WHERE user=?"
            cursor.execute(sql, (self.get_argument("level"),
                                 self.get_argument("username")))
            conn.commit()
            conn.close()
            self.set_secure_cookie("alert_msg", "Пользователь изменён")
            self.set_secure_cookie("alert_msg_status", "success")
            self.redirect(u"/settings")

        if self.get_query_argument("change_user_pass", default=None) == "":
            if self.get_argument("password1") != self.get_argument("password2"):
                self.set_secure_cookie("alert_msg", "Пароли не совпадают")
                self.set_secure_cookie("alert_msg_status", "danger")
                self.redirect(u'/settings')

            elif len(self.get_argument("password1")) <= 6:
                self.set_secure_cookie("alert_msg", "Пароль слишком короткий")
                self.set_secure_cookie("alert_msg_status", "danger")
                self.redirect(u"/settings")

            else:
                hash_pass = pwd_context.hash(self.get_argument("password1"))
                conn = sqlite3.connect(DB_PATH, timeout=10)
                cursor = conn.cursor()
                sql = "UPDATE users SET password=? WHERE user=?"
                try:
                    cursor.execute(sql, (hash_pass, self.get_argument("username")))
                except Exception as err:
                    if "UNIQUE constraint failed" in str(err):
                        self.set_secure_cookie("alert_msg",
                                               "Пользователь с таким именем уже существует")
                        self.set_secure_cookie("alert_msg_status", "danger")
                        self.redirect(u"/settings")
                    else:
                        self.set_secure_cookie("alert_msg", "Произошла внутренняя ошибка")
                        self.set_secure_cookie("alert_msg_status", "danger")
                        self.redirect(u"/settings")
                conn.commit()
                conn.close()
                self.set_secure_cookie("alert_msg", "Пользователь изменён")
                self.set_secure_cookie("alert_msg_status", "success")
                self.redirect(u"/settings")

        if self.get_query_argument("settings_smtp", default=None) == "":
            smtp_status = self.get_argument("smtp_status", default=0)
            smtp_server = self.get_argument("smtp_server")
            smtp_port = self.get_argument("smtp_port")
            smtp_login = self.get_argument("smtp_login")
            smtp_password = self.get_argument("smtp_password")

            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()
            sql = """UPDATE settings_smtp SET status=?, server=?, port=?, login=?, password=?
                     WHERE id=0"""
            cursor.execute(sql, (smtp_status, smtp_server, smtp_port, smtp_login, smtp_password))
            conn.commit()
            conn.close()

            self.set_secure_cookie("alert_msg", "Данные обновлены")
            self.set_secure_cookie("alert_msg_status", "success")
            self.redirect(u"/settings")

        if self.get_query_argument("settings_telegram", default=None) == "":
            tg_status = self.get_argument("tg_status", default=0)
            tg_token = self.get_argument("tg_token")
            tg_proxy_status = self.get_argument("tg_proxy_status", default=0)
            tg_proxy_proto = self.get_argument("tg_proxy_proto")
            tg_proxy_server = self.get_argument("tg_proxy_server")
            tg_proxy_port = self.get_argument("tg_proxy_port")
            tg_proxy_login = self.get_argument("tg_proxy_login")
            tg_proxy_password = self.get_argument("tg_proxy_password")

            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()
            sql = """UPDATE settings_telegram
                     SET status=?, token=?, proxy_status=?, proxy_proto=?,
                         proxy_server=?, proxy_port=?, proxy_login=?, proxy_password=?
                     WHERE id=0"""
            cursor.execute(sql, (tg_status, tg_token, tg_proxy_status, tg_proxy_proto,
                                 tg_proxy_server, tg_proxy_port, tg_proxy_login, tg_proxy_password))
            conn.commit()
            conn.close()

            self.set_secure_cookie("alert_msg", "Данные обновлены")
            self.set_secure_cookie("alert_msg_status", "success")
            self.redirect(u"/settings")

        if self.get_query_argument("add_service", default=None) == "":
            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()
            sql = "INSERT INTO services (name,reboot) VALUES (?,?)"
            try:
                cursor.execute(sql, (self.get_argument("service_name"),
                                     self.get_argument("reboot")))
            except Exception as err:
                if "UNIQUE constraint failed" in str(err):
                    self.set_secure_cookie("alert_msg",
                                           "Служба с таким именем уже существует")
                    self.set_secure_cookie("alert_msg_status", "danger")
                    self.redirect(u"/settings")
                else:
                    self.set_secure_cookie("alert_msg", "Произошла внутренняя ошибка")
                    self.set_secure_cookie("alert_msg_status", "danger")
                    self.redirect(u"/settings")
            conn.commit()
            conn.close()
            self.set_secure_cookie("alert_msg", "Служба добавлена")
            self.set_secure_cookie("alert_msg_status", "success")
            self.redirect(u"/settings")

        if self.get_query_argument("delete_service", default=None) == "":
            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()

            sql = "DELETE FROM services WHERE name=?"
            cursor.execute(sql, (self.get_argument("name"),))
            conn.commit()
            conn.close()

            self.set_status(200, 'success')

        if self.get_query_argument("change_service", default=None) == "":
            conn = sqlite3.connect(DB_PATH, timeout=10)
            cursor = conn.cursor()

            sql = "UPDATE services SET reboot=? WHERE name=?"
            cursor.execute(sql, (self.get_argument("reboot"), self.get_argument("name")))
            conn.commit()
            conn.close()

            self.set_status(200, 'success')


class EventsHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        conn = sqlite3.connect(DB_PATH, timeout=10)
        cursor = conn.cursor()

        sql = """SELECT datetime, hostname, description, alert_tg_status,
                        alert_tg_desc, alert_email_status, alert_email_desc
                FROM events"""
        cursor.execute(sql)
        events = cursor.fetchall()

        self.render("events.html", tab_active="events", events=events)


class ServicesHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        conn = sqlite3.connect(DB_PATH, timeout=10)
        cursor = conn.cursor()

        sql = "SELECT name, status, reboot FROM services"
        cursor.execute(sql)
        services = cursor.fetchall()

        self.render("services.html", tab_active="services", services=services)


class TopHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        # top cpu usage
        cpu = []
        for p in sorted(psutil.process_iter(attrs=['pid', 'name', 'cpu_percent',
                                                   'status', 'cmdline']),
                        key=lambda p: p.info['cpu_percent'])[-10:]:
            cpu.append((p.info['pid'], p.info['name'], "%.2f" % p.info['cpu_percent'],
                        p.info['status'], " ".join(p.info['cmdline'])))

        # top memory usage
        mem = []
        for p in sorted(psutil.process_iter(attrs=['pid', 'name', 'memory_percent',
                                                   'status', 'cmdline']),
                        key=lambda p: p.info['memory_percent'])[-10:]:
            mem.append((p.info['pid'], p.info['name'], "%.2f" % p.info['memory_percent'],
                        p.info['status'], " ".join(p.info['cmdline'])))

        # zombie proc
        zombie = []
        for p in psutil.process_iter(attrs=['pid', 'name', 'status', 'ppid', 'cmdline']):
            if 'zombie' in p.info['status']:
                zombie.append((p.info['pid'], p.info['name'], p.info['status'],
                               p.info['ppid'], " ".join(p.info['cmdline'])))

        # i/o proc
        io_tmp = []
        for p in psutil.process_iter(attrs=['pid', 'name', 'io_counters', 'cmdline']):
            if p.info['io_counters'] is not None:
                io_tmp.append(p)
        io = []
        for p in sorted(io_tmp, key=lambda p: p.info['io_counters'] and
                        p.info['io_counters'][:2])[-10:]:
            io.append((p.info['pid'], p.info['name'], p.info['io_counters'][0],
                       p.info['io_counters'][1], " ".join(p.info['cmdline'])))

        # top inode
        fds_tmp = []
        for p in psutil.process_iter(attrs=['pid', 'name', 'num_fds', 'cmdline']):
            if p.info['num_fds'] is not None:
                fds_tmp.append(p)
        fds = []
        for p in sorted(fds_tmp, key=lambda p: p.info['num_fds'])[-10:]:
            fds.append((p.info['pid'], p.info['name'],
                        p.info['num_fds'], " ".join(p.info['cmdline'])))

        top = {
            "cpu": reversed(cpu),
            "mem": reversed(mem),
            "zombie": zombie,
            "io": reversed(io),
            "fds": reversed(fds)
        }
        self.render("top.html", tab_active="top", top=top)


class NetworkHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        interfaces = ifconfig.main()
        af_map = {
            socket.AF_INET: 'IPv4',
            socket.AF_INET6: 'IPv6',
            psutil.AF_LINK: 'MAC',
        }
        listen = []
        for item in psutil.net_connections():
            if af_map.get(item.status, item.status) == "LISTEN":
                proto = af_map.get(item.family, item.family)
                ip = af_map.get(item.laddr.ip, item.laddr.ip)
                port = af_map.get(item.laddr.port, item.laddr.port)
                pid = af_map.get(item.pid, item.pid)
                listen.append((proto, ip, port, pid))
        self.render("network.html", tab_active="network", interfaces=interfaces, listen=listen)


class GetInfoHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        if self.get_argument("stat", default=None, strip=False) == 'top':
            response = sys_info.top()
            self.write(json.dumps(response))

        if self.get_argument("stat", default=None, strip=False) == 'system_load':
            age = self.get_argument("age", default=None, strip=False)
            response = sys_info.system_load(age)
            self.write(json.dumps(response))

        if self.get_argument("stat", default=None, strip=False) == 'mem_load':
            response = sys_info.mem_load()
            self.write(json.dumps(response))

        if self.get_argument("stat", default=None, strip=False) == 'swap_load':
            response = sys_info.swap_load()
            self.write(json.dumps(response))

        if self.get_argument("stat", default=None, strip=False) == 'memory_usage':
            age = self.get_argument("age", default=None, strip=False)
            response = sys_info.memory_usage(age)
            self.write(json.dumps(response))

        if self.get_argument("stat", default=None, strip=False) == 'disks':
            device = self.get_argument("device", default=None, strip=False)
            data_type = self.get_argument("data_type", default=None, strip=False)
            age = self.get_argument("age", default=None, strip=False)
            response = sys_info.disks(device, data_type, age)
            self.write(json.dumps(response))


def main():
    # get config from file
    define("PORT", type=int)
    define("COOKIE_SECRET", type=str)
    define("DEBUG", type=bool)
    parse_config_file(os.path.dirname(os.path.abspath(__file__)) + "/easymon.conf")

    app = tornado.web.Application(
        [
            (r'/', MainHandler),
            (r'/login', LoginHandler),
            (r'/logout', LogoutHandler),
            (r'/settings', SettingsHandler),
            (r'/events', EventsHandler),
            (r'/services', ServicesHandler),
            (r'/top', TopHandler),
            (r'/network', NetworkHandler),
            (r'/_get', GetInfoHandler)
        ],
        cookie_secret=options.COOKIE_SECRET,
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        xsrf_cookies=True,
        login_url="/login",
        debug=options.DEBUG
    )
    app.listen(options.PORT)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
